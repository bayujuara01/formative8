import java.io.File;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Logger;

public class FormativeTask3 {
    private final Logger logger = Logger.getLogger(FormativeTask3.class.getName());
    private String text = "";

    public FormativeTask3() {
        setText();
    }

    public void run() {
        PrintWriter printWriter;
        File fileDay821 = new File("./file821.txt");
        File fileDay822 = new File("./file822.txt");

        text = text.replaceAll("L", "7")
            .replaceAll("i", "1")
            .replaceAll("p", "8")
            .replaceAll("o", "9")
            .replaceAll("s", "5");
        
        
        try {
            printWriter = new PrintWriter(fileDay821, "UTF-8");
            printWriter.write(text.replaceAll("\\D", ""));
            printWriter.close();

            printWriter = new PrintWriter(fileDay822, "UTF-8");
            printWriter.write(text.replaceAll("\\d", ""));
            printWriter.close();

            logger.info("Program has successfully peformed!");
        } catch (Exception e) {
            logger.warning("Program has an error, please try again");
        }
    }

    private void setText() {
        text = "Lorem ipsum dolor sit amet, Lonsectetur adipiscing elit. "
            + "Maecenas dictum nisl sed mi tempus tristique. Suspendisse eu"
            + " convallis nunc. In hac habitasse platea dictumst. Mauris "
            + "placerat quis nulla ac rhoncus. Mauris blandit nisl sit amet"
            + " facilisis rhoncus. Suspendisse et posuere neque. Quisque sapien"
            + " lectus, posuere id nulla eget, convallis mollis tellus. Duis"
            + " convallis sapien ut nunc facilisis fermentum. Nulla placerat"
            + " massa quis mattis rutrum. Interdum et malesuada fames ac ante"
            + " ipsum primis in faucibus. Aenean scelerisque luctus eros ac volutpat..";
    }

    public static void main(String[] args) {
        FormativeTask3 task3 = new FormativeTask3();
        task3.run();
    }
}
