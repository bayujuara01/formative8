public class FormativeTask2 {
    private String text = "";

    public FormativeTask2() {
        setText();
    }

    public void run() {
        text = text.replaceAll("L", "7")
            .replaceAll("i", "1")
            .replaceAll("p", "8")
            .replaceAll("o", "9")
            .replaceAll("s", "5");
        
        System.out.println(text);
    }

    private void setText() {
        text = "Lorem ipsum dolor sit amet, Lonsectetur adipiscing elit. "
            + "Maecenas dictum nisl sed mi tempus tristique. Suspendisse eu"
            + " convallis nunc. In hac habitasse platea dictumst. Mauris "
            + "placerat quis nulla ac rhoncus. Mauris blandit nisl sit amet"
            + " facilisis rhoncus. Suspendisse et posuere neque. Quisque sapien"
            + " lectus, posuere id nulla eget, convallis mollis tellus. Duis"
            + " convallis sapien ut nunc facilisis fermentum. Nulla placerat"
            + " massa quis mattis rutrum. Interdum et malesuada fames ac ante"
            + " ipsum primis in faucibus. Aenean scelerisque luctus eros ac volutpat..";
    }

    public static void main(String[] args) {
        FormativeTask2 task2 = new FormativeTask2();
        task2.run();
    }
}
