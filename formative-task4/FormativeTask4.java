import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Scanner;

/**
 * @author Bayu Seno Ariefyanto
 * @version 1.0
 * @since 06/10/2021 22:03 GMT +7
 */

public class FormativeTask4 {
    public static Locale indonesia = new Locale("id", "ID");
    public static NumberFormat numberFormat = NumberFormat.getCurrencyInstance(indonesia);
    public static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd MMMM yyyy", indonesia);

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        final int monthlyPrice = 153000;
        String dateString = "";
        String[] dateStringArr;
        boolean isDateValid = false;
        LocalDate registerDate;

        do {
            System.out.print("Masukan Tanggal Registrasi [dd/mm/yyyy] : ");
            dateString = scanner.nextLine();
            isDateValid = dateString.matches("[0-3][1-9]/[0-3][0-9]/[0-9]{4}");

            if (!isDateValid) {
                System.out.println("[Mohon masukkan tanggal sesuai format]");
            }

        } while (!isDateValid);

        dateStringArr = dateString.split("/");
        registerDate = LocalDate.parse(String.format("%s-%s-%s", dateStringArr[2], dateStringArr[1], dateStringArr[0]));
        LocalDate now = LocalDate.now();
        
        if (registerDate.equals(LocalDate.now())) {
            showInvoice(registerDate, monthlyPrice, "Pembayaran Bulan Ini Terbayar !");
        } else if (now.isAfter(registerDate)) {
            showInvoice(registerDate.plusMonths(1), monthlyPrice, "Bulan ini sudah lunas, bayar bulan depan pada : ");
        } else {
            showInvoice(registerDate, monthlyPrice, "Pembayaran Bulan ini akan dibayar pada : ");     
        }
    }

    public static void showInvoice(LocalDate registerDate, int monthlyPrice, String comment) {
        System.out.println("\nNETFLIX INVOICE");
        System.out.println(comment);
        System.out.printf("Sebesar : %s\n", numberFormat.format(monthlyPrice));
        System.out.printf("Tanggal : %s", dateTimeFormatter.format(registerDate));
    }
}
