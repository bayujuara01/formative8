import java.time.Duration;
import java.time.LocalTime;
import java.util.Scanner;

public class Main {
    private final String idPattern = "NIK-[0]{2}-\\d+";
    private final String timePattern = "[0-9]{2}:[0-9]{2}(:[0-9]{2})?";
    private final LocalTime timeTolerance = LocalTime.of(8, 15);
    private LocalTime time = null;
    private String id = "";

    public void run(Scanner scanner) {
        boolean isIdValid = false;
        boolean isTimeValid = false;
        String timeString = "";

        println("========ABSENSI==========");
        do {
            print("Masukkan NIK, contoh [NIK-00-number] : ");
            id = scanner.nextLine();
            isIdValid = checkIdValid(id);

            if (!isIdValid) {
                println("Maaf input NIK tidak sesuai [NIK-00-number]!");
            }
        } while (!isIdValid);

        do {
            print("Masukkan Waktu, contoh [00:00:00 atau 00:00] : ");
            timeString = scanner.nextLine();
            isTimeValid =  checkTimeValid(timeString);

            if (!isTimeValid) {
                println("Maaf input waktu tidak sesuai [00:00:00 atau 00:00]!");
            }

        } while (!isTimeValid);

        absensi(id, timeString);
    }

    private void absensi(String id, String timeString) {
        this.time = LocalTime.parse(timeString);
        Duration overdue = Duration.between(timeTolerance, time);

        if (overdue.toMinutes() > 15) {
            System.out.printf("\nNIK: '%s'\nTerlambat: 'Terlambat'\nWaktuKeterlambatan: %d\n", id, overdue.toMinutes());
        } else {
            System.out.printf("\nNIK: '%s'\nTerlambat: 'Tidak'\nWaktuKeterlambatan: 0\n", id);
        }
    }

    private boolean checkTimeValid(String time) {
        return time.matches(timePattern);
    }

    private boolean checkIdValid(String nik) {
        return nik.matches(idPattern);
    }

    private void println(String str) {
        System.out.println(str);
    }

    private void print(String str) {
        System.out.print(str);
    }

    public static void main(String[] args) {
        Main app = new Main();
        app.run(new Scanner(System.in));
    }
}