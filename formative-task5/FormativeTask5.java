import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Scanner;

/**
 * @author Bayu Seno Ariefyanto
 * @version 2.3
 * @since 06/10/2021 23:23 GMT +7
 */

public class FormativeTask5 {
    public static Locale indonesia = new Locale("id", "ID");
    public static NumberFormat numberFormat = NumberFormat.getCurrencyInstance(indonesia);
    public static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("EEEE, dd MMMM yyyy", indonesia);

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        final int monthlyPrice = 153000;
        String dateString = "";
        String reminderDay = "";
        String[] dateStringArr;
        boolean isDateValid = false;
        boolean isReminderDayValid = false;
        LocalDate registerDate;

        do {
            System.out.print("Masukan Tanggal Registrasi [dd/mm/yyyy] : ");
            dateString = scanner.nextLine();
            isDateValid = dateString.matches("[0-3][1-9]/[0-3][0-9]/[0-9]{4}");

            if (!isDateValid || !isReminderDayValid) {
                System.out.println("[Mohon masukkan tanggal sesuai format]");
            }
        } while (!isDateValid);

        do {
            System.out.print("Masukan Rentang Hari Email Reminder Pembayaran 1 s.d 7 hari [1-7] : ");
            reminderDay = scanner.nextLine();
            isReminderDayValid = reminderDay.matches("[1-7]{1}");

            if (!isReminderDayValid) {
                System.out.println("[Mohon masukkan tanggal sesuai format]");
            }
        } while (!isReminderDayValid);

        dateStringArr = dateString.split("/");
        registerDate = LocalDate.parse(String.format("%s-%s-%s", dateStringArr[2], dateStringArr[1], dateStringArr[0]));

        for (LocalDate date = registerDate; date.isBefore(registerDate.plusYears(1)); date = date.plusDays(1)) {
            if (date.getDayOfMonth() == registerDate.minusDays(Integer.parseInt(reminderDay)).getDayOfMonth()) {
                sendEmail(date);
                showInvoice(date.plusDays(Integer.parseInt(reminderDay)), monthlyPrice, "Jatuh Tempo Pembayaran : ");
            }
        }

        scanner.close();
    }

    public static void showInvoice(LocalDate registerDate, int monthlyPrice, String comment) {
        System.out.println("NETFLIX INVOICE");
        System.out.println(comment);
        System.out.printf("Sebesar : %s\n", numberFormat.format(monthlyPrice));
        System.out.printf("Tanggal : %s\n", dateTimeFormatter.format(registerDate));
    }
    
    public static void sendEmail(LocalDate date) {
        System.out.printf("\n%s -> email reminder dikirim ke pelanggan\n", dateTimeFormatter.format(date));
    }
}
